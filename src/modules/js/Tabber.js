/*global jQuery:false, console:false */

var Tabber = (function($) {
	'use strict'; 

	function Tabber() {
		this.tab            = 'data-tab';
		this.tabber         = 'data-tabber';
		this.link           = 'data-tab-link';
		this.tabContainer   = 'data-tabber-content';
		this.onBeforeChange = 'data-onBeforeChange';
		this.onChange       = 'data-onChange';

		this.activeClass    = '.active';

		this.Init();
		this.Events();
		var _this = this;
	}

	Tabber.prototype.Init = function() {
		var _this = this;

		$('[' + this.tabContainer + ']').each( function(index, item) {

			var tabber = $(this),
			tabs       = tabber.children('[' + _this.tab + ']'),
			activeTab  = tabber.children(_this.activeClass);

			_this.FixedHeight( tabber, tabs );

			
			$('[' + _this.tab  + '=' + activeTab.attr(_this.link)  + ']').addClass( _this.activeClass.substring(1) ); // add "active" class to current tab

			if ( !activeTab.length ) {
				tabs.first().addClass( _this.activeClass.substring(1) );
				
				$('[' + _this.link  + '=' + tabs.first().attr(_this.tab)  + ']').addClass(  _this.activeClass.substring(1) ); // add "active" class to current trigger
			} else {
				$('[' + _this.link  + '=' + activeTab.attr(_this.tab)  + ']').addClass(  _this.activeClass.substring(1) ); // add "active" class to current trigger
			}
		});
	};

	Tabber.prototype.Events = function() {
		var _this = this;

		$(document).on( 'click', '[' + this.link + ']', function(e) {
			e.preventDefault();
			_this.changeTab($(this));
		});

		$(window).on( 'load', function() {
			_this.OpenTabByHash();
		});

		$(window).on( 'resize', function() {
			_this.Init();
		});
	};

	Tabber.prototype.changeTab = function (item) {
		var tabberId = item.attr( this.tabber ),
		clickedTabId = item.attr( this.link ),
		tabber       = $('[' + this.tabContainer + '=' + tabberId + ']');

		if ( tabber === 'undefined' || !tabber.length )
			return;

		$('[' + this.tabber + '=' + tabberId + ']').removeClass( this.activeClass.substring(1) );
		tabber.children( this.activeClass ).removeClass( this.activeClass.substring(1) );

		var tab = tabber.children('[' + this.tab + '=' + clickedTabId + ']');

		if ( tab.length ) {
			this.InvokeCallback( tabber.attr(this.onBeforeChange) );

			item.addClass( this.activeClass.substring(1) );
			tab.addClass( this.activeClass.substring(1) );
			document.location.hash = clickedTabId;

			this.InvokeCallback( tabber.attr(this.onChange) );
		}
	};

	Tabber.prototype.InvokeCallback = function ( callback ) {
		callback  = window[callback];

		if ( typeof callback === 'undefined' )
			return;

		callback();
	};

	Tabber.prototype.OpenTabByHash = function() {
		if ( ! document.location.hash )
			return;

		var hash = document.location.hash.substring(1);

		$('[' + this.link + '=' + hash + ']').trigger('click');
	};


	Tabber.prototype.FixedHeight = function(tabber, tabs) {

		var heights = tabs.map(function () {
			return $(this).outerHeight();
		}).get(),

		maxHeight = Math.max.apply(null, heights);

		tabber.height(maxHeight);
	};

	return Tabber;
})(jQuery);